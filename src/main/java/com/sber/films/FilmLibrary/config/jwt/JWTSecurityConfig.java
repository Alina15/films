package com.sber.films.FilmLibrary.config.jwt;


import com.sber.films.FilmLibrary.service.userdetails.CustomUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.sber.films.FilmLibrary.constants.SecurityRequestsConstants.*;
import static com.sber.films.FilmLibrary.constants.UsersRolesConstants.LIBRARIAN;
import static com.sber.films.FilmLibrary.constants.UsersRolesConstants.USER;


@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor
public class JWTSecurityConfig {
    @Value("${spring.security.user.roles}")
    private String adminRole;
    private final JWTTokenFilter jwtTokenFilter;
    private final CustomUserDetailsService customUserDetailsService;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.cors(AbstractHttpConfigurer::disable)
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests((requests) -> requests
                                .requestMatchers(RESOURCES_WHITE_LIST.toArray(String[]::new)).permitAll()
                                .requestMatchers(FILMS_REST_WHITE_LIST.toArray(String[]::new)).permitAll()
                                .requestMatchers(DIRECTOR_WHITE_LIST.toArray(String[]::new)).permitAll()
                                .requestMatchers(USERS_WHITE_LIST.toArray(String[]::new)).permitAll()
                                .requestMatchers(FILMS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(adminRole, LIBRARIAN)
                        .requestMatchers(DIRECTOR_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(adminRole, LIBRARIAN)
                                .requestMatchers(USERS_PERMISSION_LIST.toArray(String[]::new)).hasAnyRole(USER, LIBRARIAN)
                                .requestMatchers("/rest/films/addFilm").hasRole(adminRole)
                                .requestMatchers("/rest/director/addDirector").hasRole(adminRole)
                )
                .sessionManagement(
                        session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                )
                .addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class)
                .userDetailsService(customUserDetailsService);

        return http.build();
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }
}


