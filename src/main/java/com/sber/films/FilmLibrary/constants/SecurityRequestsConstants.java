package com.sber.films.FilmLibrary.constants;

import java.util.List;

public interface SecurityRequestsConstants {
    List<String> RESOURCES_WHITE_LIST = List.of("/resources/**",
            "/static/**",
            "/js/**",
            "/css/**",
            "/",
            "/swagger-ui/**",
            "/webjars/bootstrap/5.0.2/**",
            "/v3/api-docs/**");


    List<String> FILMS_REST_WHITE_LIST = List.of("/rest/films/list",
            "/rest/films/getFilm/**");

    List<String> DIRECTOR_WHITE_LIST = List.of("/rest/director",
//            "/authors/search",
//            "/films/search/author",
            "/rest/director/{id}");
    List<String> FILMS_PERMISSION_LIST = List.of("/rest/films/add",
            "/rest/films/addFilm",
            "/rest/films/updateFilm",
            "/rest/films/deleteFilm/{id}",
            "/rest/films/addDirector",
            "/rest/films/update/*",
            "/rest/films/delete/*");

    List<String> DIRECTOR_PERMISSION_LIST = List.of("/rest/director/add",
            "/rest/director/addDirector",
            "/rest/director/updateDirector",
            "/rest/director/addFilm",
            "/rest/director/deleteDirector/{id}",
            "/rest/director/update/*",
            "/rest/director/delete/*");


    List<String> USERS_WHITE_LIST = List.of("/login",
            "/users/registration",
            "/users/remember-password",
            "/users/change-password",
            "/users/auth");

    List<String> USERS_PERMISSION_LIST = List.of(
            "/rest/order/addOrder",
            "/rest/order/updateOrder",
            "/rest/order/deleteOrder/{id}",
            "/rest/order/listOrderedFilmsByUser",
            "/rest/order/update/*",
            "/rest/order/delete/*");
}

