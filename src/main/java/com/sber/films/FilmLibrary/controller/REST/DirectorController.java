package com.sber.films.FilmLibrary.controller.REST;


import com.sber.films.FilmLibrary.dto.AddFilmOrDirectorDto;
import com.sber.films.FilmLibrary.dto.DirectorDto;
import com.sber.films.FilmLibrary.dto.create.CreateDirectorRequest;
import com.sber.films.FilmLibrary.exception.NotFoundException;
import com.sber.films.FilmLibrary.service.DirectorService;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/rest/director")
@RequiredArgsConstructor
@SecurityRequirement(name = "Bearer Authentication")
@Slf4j
public class DirectorController {
    private final DirectorService directorService;
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DirectorDto>> getAllDirectors() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(directorService.listAll());
    }

    @RequestMapping(value = "/getDirector", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDto> getOneDirector(@RequestParam(value = "id") Long id) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(directorService.getOne(id));
    }

    @RequestMapping(value = "/addDirector", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDto> create(@RequestBody CreateDirectorRequest newDirector) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(directorService.createDirectorByRequest(newDirector));
    }

    @RequestMapping(value = "/updateDirector", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDto> update(@RequestBody DirectorDto updatedDirector,
                                          @RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(directorService.update(updatedDirector, id));
    }

    @RequestMapping(value = "/deleteDirector/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        directorService.delete(id);
    }



    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DirectorDto> addFilm(@RequestBody AddFilmOrDirectorDto addFilmOrDirectorDto) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.CREATED).body(directorService.addFilm(addFilmOrDirectorDto));
    }
}
