package com.sber.films.FilmLibrary.controller.REST;

import com.sber.films.FilmLibrary.dto.AddFilmOrDirectorDto;
import com.sber.films.FilmLibrary.dto.FilmDto;
import com.sber.films.FilmLibrary.dto.create.CreateFilmRequest;
import com.sber.films.FilmLibrary.exception.NotFoundException;
import com.sber.films.FilmLibrary.service.FilmService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/films")
@RequiredArgsConstructor
@Slf4j
@SecurityRequirement(name = "Bearer Authentication")
public class FilmController {
    private final FilmService filmService;
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FilmDto>> getAllFilms() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(filmService.listAll());
    }
    @RequestMapping(value = "/getFilm", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDto> getOneFilm(@RequestParam(value = "id") Long id) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(filmService.getOne(id));
    }

    @RequestMapping(value = "/addFilm", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDto> create(@RequestBody CreateFilmRequest newFilm) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(filmService.createFilmByRequest(newFilm));
    }

    @RequestMapping(value = "/updateFilm", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDto> update(@RequestBody FilmDto updatedFilm,
                                          @RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(filmService.update(updatedFilm, id));
    }

    @RequestMapping(value = "/deleteFilm/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        filmService.delete(id);
    }



    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FilmDto> addDirector(@RequestBody AddFilmOrDirectorDto addFilmOrDirectorDto) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.CREATED).body(filmService.addDirector(addFilmOrDirectorDto));
    }

}
