package com.sber.films.FilmLibrary.controller.REST;


import com.sber.films.FilmLibrary.dto.FilmDto;
import com.sber.films.FilmLibrary.dto.OrderDto;
import com.sber.films.FilmLibrary.dto.UserDto;
import com.sber.films.FilmLibrary.dto.create.CreateOrderRequest;
import com.sber.films.FilmLibrary.dto.create.CreateUserRequest;
import com.sber.films.FilmLibrary.exception.NotFoundException;
import com.sber.films.FilmLibrary.model.Director;
import com.sber.films.FilmLibrary.model.Order;
import com.sber.films.FilmLibrary.repository.DirectorRepository;
import com.sber.films.FilmLibrary.repository.OrderRepository;
import com.sber.films.FilmLibrary.service.OrderService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/rest/order")
@RequiredArgsConstructor
@Slf4j
@SecurityRequirement(name = "Bearer Authentication")
public class OrderController {
    private final OrderService orderService;
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<OrderDto>> getAllOrders() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(orderService.listAll());
    }

    @RequestMapping(value = "/getOrder", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDto> getOneOrder(@RequestParam(value = "id") Long id) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(orderService.getOne(id));
    }
    @RequestMapping(value = "/listOrderedFilmsByUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FilmDto>> getAllOrderedFilmsByUser(@RequestParam(value = "id") Long id) throws NotFoundException  {
        return ResponseEntity.status(HttpStatus.OK)
                .body(orderService.getOrderedFilmsByUser(id));
    }

    @RequestMapping(value = "/addOrder", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDto> create(@RequestBody CreateOrderRequest newOrder) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(orderService.createOrderByRequest(newOrder));
    }

    @RequestMapping(value = "/updateOrder", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDto> update(@RequestBody OrderDto updatedOrder,
                                           @RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(orderService.update(updatedOrder, id));
    }

    @RequestMapping(value = "/deleteOrder/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        orderService.delete(id);
    }

}
