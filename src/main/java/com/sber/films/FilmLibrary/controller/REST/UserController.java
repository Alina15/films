package com.sber.films.FilmLibrary.controller.REST;


import com.sber.films.FilmLibrary.config.jwt.JWTTokenUtil;
import com.sber.films.FilmLibrary.dto.LoginDto;
import com.sber.films.FilmLibrary.service.UserService;
import com.sber.films.FilmLibrary.service.userdetails.CustomUserDetailsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController()
@RequestMapping("/users")
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final CustomUserDetailsService customUserDetailsService;
    private final UserService userService;
    private final JWTTokenUtil tokenUtil;

    @PostMapping("/auth")
    public ResponseEntity<?> auth(@RequestBody LoginDto loginDto) {
        Map<String, Object> response = new HashMap<>();
        UserDetails foundUser = customUserDetailsService.loadUserByUsername(loginDto.getLogin());
        log.info("Found User: " + foundUser);
        if (!userService.checkPassword(loginDto.getPassword(), foundUser)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Ошибка авторизации. Неверный пароль.");
        }
        final String token = tokenUtil.generateToken(foundUser);
        response.put("token", token);
        response.put("username", foundUser.getUsername());
        response.put("roles", foundUser.getAuthorities());
        return ResponseEntity.ok().body(response);
    }
}

