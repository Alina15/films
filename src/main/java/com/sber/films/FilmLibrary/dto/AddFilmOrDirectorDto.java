package com.sber.films.FilmLibrary.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class AddFilmOrDirectorDto {
    Long filmId;
    Long directorId;
}
