package com.sber.films.FilmLibrary.dto;

import jakarta.persistence.Column;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DirectorDto extends GenericDto {
    private String directorFio;
    private String position;
    private List<Long> filmIds = new ArrayList<>();
}
