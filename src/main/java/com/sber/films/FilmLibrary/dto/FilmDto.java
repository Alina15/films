package com.sber.films.FilmLibrary.dto;

import com.sber.films.FilmLibrary.model.Director;
import com.sber.films.FilmLibrary.model.Film;
import com.sber.films.FilmLibrary.model.Genre;
import com.sber.films.FilmLibrary.model.Order;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDto extends GenericDto {
    private String title;
    private LocalDate premierYear;
    private Genre genre;
    private String country;
    private List<Long> orderIds = new ArrayList<>();
    private List<Long> directorIds = new ArrayList<>();

}
