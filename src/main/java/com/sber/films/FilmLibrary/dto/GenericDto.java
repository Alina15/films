package com.sber.films.FilmLibrary.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class GenericDto {
    protected Long id;
    protected String createdBy;
    protected LocalDateTime createdWhen;
    protected String updatedBy;
    protected LocalDateTime updatedWhen;
}

