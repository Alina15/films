package com.sber.films.FilmLibrary.dto;

import jakarta.persistence.Column;
import lombok.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto extends GenericDto {
    private LocalDate rentDate;
    private LocalDate returnPeriod;
    private Boolean purchase;
    private Long filmId;
    private Long userId;

}
