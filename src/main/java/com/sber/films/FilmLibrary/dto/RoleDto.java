package com.sber.films.FilmLibrary.dto;

import jakarta.persistence.Column;
import lombok.*;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto{
    private Long id;
    private String title;
    private String description;

}
