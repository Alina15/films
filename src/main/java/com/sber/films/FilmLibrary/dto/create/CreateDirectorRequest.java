package com.sber.films.FilmLibrary.dto.create;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@ToString
@Data
public class CreateDirectorRequest {
    private String directorFio;
    private String position;
    private List<Long> filmIds = new ArrayList<>();
}
