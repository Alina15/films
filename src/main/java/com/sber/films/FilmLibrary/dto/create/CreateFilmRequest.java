package com.sber.films.FilmLibrary.dto.create;

import com.sber.films.FilmLibrary.dto.GenericDto;
import com.sber.films.FilmLibrary.model.Genre;
import lombok.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ToString
@Data
public class CreateFilmRequest {

        private String title;
        private LocalDate premierYear;
        private String country;
        private Genre genre;
        private List<Long> orderIds = new ArrayList<>();
        private List<Long> directorIds = new ArrayList<>();


}
