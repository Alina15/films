package com.sber.films.FilmLibrary.dto.create;

import lombok.Data;
import lombok.ToString;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ToString
@Data
public class CreateOrderRequest {
    private LocalDate rentDate;
    private LocalDate returnPeriod;
    private Boolean purchase;
    private Long filmId;
    private Long userId;
}
