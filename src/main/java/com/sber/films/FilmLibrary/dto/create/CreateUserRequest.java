package com.sber.films.FilmLibrary.dto.create;

import com.sber.films.FilmLibrary.dto.RoleDto;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ToString
@Data
public class CreateUserRequest {
    private String login;
    private String password;
    private String email;
    private String firstName;
    private String middleName;
    private String lastName;
    private LocalDate birthDate;
    private String phone;
    private String address;
    private Long roleId;
    private List<Long> orderIds = new ArrayList<>();
}
