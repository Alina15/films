package com.sber.films.FilmLibrary.dto.error;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FieldErrorDto {
    private final String objectName;
    private final String field;
    private final String message;
}

