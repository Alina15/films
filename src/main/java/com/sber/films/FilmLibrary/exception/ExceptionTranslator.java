package com.sber.films.FilmLibrary.exception;

import com.sber.films.FilmLibrary.constants.Error;
import com.sber.films.FilmLibrary.dto.error.ErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.security.sasl.AuthenticationException;
import java.nio.file.AccessDeniedException;

//@ControllerAdvice
@RestControllerAdvice
public class ExceptionTranslator {
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleNotFoundException(NotFoundException ex) {
        return proceedFieldsErrors(ex, Error.REST.NOT_FOUND_ERROR_MESSAGE, ex.getMessage());
    }

    @ExceptionHandler(MyDeleteException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleMyDeleteException(MyDeleteException ex) {
        return proceedFieldsErrors(ex, Error.REST.DELETE_ERROR_MESSAGE, ex.getMessage());
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ErrorDto handleAuthException(AccessDeniedException ex) {
        return proceedFieldsErrors(ex, Error.REST.AUTH_ERROR_MESSAGE, ex.getMessage());
    }

    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorDto handleAuthException(AuthenticationException ex) {
        return proceedFieldsErrors(ex, Error.REST.ACCESS_ERROR_MESSAGE, ex.getMessage());
    }

    private ErrorDto proceedFieldsErrors(Exception ex,
                                         String error,
                                         String description) {
        ErrorDto errorDTO = new ErrorDto(error, description);
        errorDTO.add(ex.getClass().getName(), "", errorDTO.getMessage());
        return errorDTO;
    }
}

