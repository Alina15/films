package com.sber.films.FilmLibrary.exception;

public class MyDeleteException extends Exception {
    public MyDeleteException(final String message) {
        super(message);
    }
}

