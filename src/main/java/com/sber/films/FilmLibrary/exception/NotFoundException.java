package com.sber.films.FilmLibrary.exception;

public class NotFoundException extends Exception {
    public NotFoundException(final String message) {
        super(message);
    }
}

