package com.sber.films.FilmLibrary.mapper;

import com.sber.films.FilmLibrary.dto.DirectorDto;
import com.sber.films.FilmLibrary.model.Director;
import com.sber.films.FilmLibrary.model.GenericModel;
import com.sber.films.FilmLibrary.repository.FilmRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class DirectorMapper extends GenericMapperImpl<Director, DirectorDto> {
    private final FilmRepository filmRepository;
    public DirectorMapper(ModelMapper modelMapper,
                          FilmRepository filmRepository) {
        super(Director.class, DirectorDto.class, modelMapper);
        this.filmRepository = filmRepository;
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Director.class, DirectorDto.class)
                .addMappings(m -> m.skip(DirectorDto::setFilmIds)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(DirectorDto.class, Director.class)
                .addMappings(m -> m.skip(Director::setFilms)).setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(Director source, DirectorDto destination) {
        destination.setFilmIds(fillIds(source));

    }

    @Override
    protected void mapSpecificFields(DirectorDto source, Director destination) {
        if (!Objects.isNull(source.getFilmIds())) {
            destination.setFilms(filmRepository.findAllById(source.getFilmIds()));
        } else {
            destination.setFilms(Collections.emptyList());
        }
    }

    @Override
    protected List<Long> fillIds(Director source) {
        return Objects.isNull(source) || Objects.isNull(source.getFilms()) || source.getFilms().isEmpty()
                ? Collections.EMPTY_LIST
                : source.getFilms().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
