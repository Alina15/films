package com.sber.films.FilmLibrary.mapper;

import com.sber.films.FilmLibrary.dto.FilmDto;
import com.sber.films.FilmLibrary.model.Film;
import com.sber.films.FilmLibrary.repository.DirectorRepository;
import com.sber.films.FilmLibrary.model.GenericModel;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;


import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
@Component
public class FilmMapper extends GenericMapperImpl<Film, FilmDto>{
    private final DirectorRepository directorRepository;
    public FilmMapper(ModelMapper modelMapper,
                      DirectorRepository directorRepository) {
        super(Film.class, FilmDto.class, modelMapper);
        this.directorRepository = directorRepository;
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Film.class, FilmDto.class)
                .addMappings(m -> m.skip(FilmDto::setDirectorIds)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(FilmDto.class, Film.class)
                .addMappings(m -> m.skip(Film::setDirectors)).setPostConverter(toEntityConverter());

    }
    
    @Override
    protected void mapSpecificFields(Film source, FilmDto destination) {
        destination.setDirectorIds(fillIds(source));

    }

    @Override
    protected void mapSpecificFields(FilmDto source, Film destination) {
        if (!Objects.isNull(source.getDirectorIds())) {
            destination.setDirectors(directorRepository.findAllById(source.getDirectorIds()));
        } else {
            destination.setDirectors(Collections.emptyList());
        }
    }

    @Override
    protected List<Long> fillIds(Film source) {
        return Objects.isNull(source) || Objects.isNull(source.getDirectors()) || source.getDirectors().isEmpty()
                ? Collections.EMPTY_LIST
                : source.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }

}
