package com.sber.films.FilmLibrary.mapper;

import com.sber.films.FilmLibrary.dto.GenericDto;
import com.sber.films.FilmLibrary.model.GenericModel;

import java.util.List;

public interface Mapper<E extends GenericModel, D extends GenericDto> {
    E toEntity(D dto);

    D toDto(E entity);

    List<E> toEntities(List<D> dtoList);

    List<D> toDtos(List<E> entityList);
}

