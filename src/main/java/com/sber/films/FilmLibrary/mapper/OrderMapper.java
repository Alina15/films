package com.sber.films.FilmLibrary.mapper;

import com.sber.films.FilmLibrary.dto.OrderDto;
import com.sber.films.FilmLibrary.dto.UserDto;
import com.sber.films.FilmLibrary.model.GenericModel;
import com.sber.films.FilmLibrary.model.Order;
import com.sber.films.FilmLibrary.model.User;
import com.sber.films.FilmLibrary.repository.FilmRepository;
import com.sber.films.FilmLibrary.repository.OrderRepository;
import com.sber.films.FilmLibrary.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class OrderMapper extends GenericMapperImpl<Order, OrderDto>{
    private final FilmRepository filmRepository;
     private final UserRepository userRepository;
    public OrderMapper(ModelMapper modelMapper,
                       FilmRepository filmRepository, UserRepository userRepository) {
        super(Order.class, OrderDto.class, modelMapper);
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Order.class, OrderDto.class)
                .addMappings(m -> m.skip(OrderDto::setFilmId)).setPostConverter(toDtoConverter())
                   .addMappings(m -> m.skip(OrderDto::setUserId)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(OrderDto.class, Order.class)
                .addMappings(m -> m.skip(Order::setFilm)).setPostConverter(toEntityConverter())
                  .addMappings(m -> m.skip(Order::setUser)).setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(Order source, OrderDto destination) {
        destination.setFilmId(getFilmId(source));
          destination.setUserId(getUserId(source));

    }
    private Long getFilmId(Order source) {
        return Objects.isNull(source) || Objects.isNull(source.getId()) ? null : source.getFilm().getId();
    }
    private Long getUserId(Order source) {
        return Objects.isNull(source) || Objects.isNull(source.getId()) ? null : source.getUser().getId();
    }
    @Override
    protected void mapSpecificFields(OrderDto source, Order destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElse(null));
          destination.setUser(userRepository.findById(source.getUserId()).orElse(null));
    }

    @Override
    protected List<Long> fillIds(Order source) {
        return null;
    }


}
