package com.sber.films.FilmLibrary.mapper;

import com.sber.films.FilmLibrary.dto.FilmDto;
import com.sber.films.FilmLibrary.dto.UserDto;
import com.sber.films.FilmLibrary.model.Film;
import com.sber.films.FilmLibrary.model.GenericModel;
import com.sber.films.FilmLibrary.model.Order;
import com.sber.films.FilmLibrary.model.User;
import com.sber.films.FilmLibrary.repository.DirectorRepository;
import com.sber.films.FilmLibrary.repository.OrderRepository;
import com.sber.films.FilmLibrary.repository.RoleRepository;
import com.sber.films.FilmLibrary.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapperImpl<User, UserDto>{
    private final OrderRepository orderRepository;
    private final RoleRepository roleRepository;
    public UserMapper(ModelMapper modelMapper,
                      OrderRepository orderRepository, RoleRepository roleRepository) {
        super(User.class, UserDto.class, modelMapper);
        this.orderRepository = orderRepository;
        this.roleRepository = roleRepository;
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDto.class)
                .addMappings(m -> m.skip(UserDto::setOrderIds)).setPostConverter(toDtoConverter())
                .addMappings(m -> m.skip(UserDto::setRoleId)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(UserDto.class, User.class)
                .addMappings(m -> m.skip(User::setOrderList)).setPostConverter(toEntityConverter())
                .addMappings(m -> m.skip(User::setRole)).setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(User source, UserDto destination) {
        destination.setOrderIds(fillIds(source));
        destination.setRoleId(getRoleId(source));

    }
    private Long getRoleId(User source) {
        return Objects.isNull(source) || Objects.isNull(source.getId()) ? null : source.getRole().getId();
    }
    @Override
    protected void mapSpecificFields(UserDto source, User destination) {
        if (!Objects.isNull(source.getOrderIds())) {
            destination.setOrderList(orderRepository.findAllById(source.getOrderIds()));
        } else {
            destination.setOrderList(Collections.emptyList());
        }
        destination.setRole(roleRepository.findById(source.getRoleId()).orElse(null));
    }

    @Override
    protected List<Long> fillIds(User source) {
        return Objects.isNull(source) || Objects.isNull(source.getOrderList()) || source.getOrderList().isEmpty()
                ? Collections.EMPTY_LIST
                : source.getOrderList().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
