package com.sber.films.FilmLibrary.model;

import com.sber.films.FilmLibrary.dto.FilmDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "films", schema = "public")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Film extends GenericModel{
    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private LocalDate premierYear;
    @Column(nullable = false)
    private String country;
    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private Genre genre;
    @OneToMany(mappedBy = "film", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Order> orderList;
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "films_directors",
            joinColumns = @JoinColumn(name = "film_id"), foreignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS"),
            inverseJoinColumns = @JoinColumn(name = "director_id"), inverseForeignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS")
    )
    private List<Director> directors;


}
