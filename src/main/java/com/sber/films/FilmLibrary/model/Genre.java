package com.sber.films.FilmLibrary.model;


public enum Genre {
    FANTASY,
    DRAMA,
    HISTORY,
    SCIENCE_FICTION
}

