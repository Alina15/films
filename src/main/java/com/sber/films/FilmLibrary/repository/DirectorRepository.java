package com.sber.films.FilmLibrary.repository;

import com.sber.films.FilmLibrary.model.Director;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends GenericRepository<Director>{
}
