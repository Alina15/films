package com.sber.films.FilmLibrary.repository;

import com.sber.films.FilmLibrary.model.Film;
import com.sber.films.FilmLibrary.model.Order;
import com.sber.films.FilmLibrary.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FilmRepository extends GenericRepository<Film>{
}
