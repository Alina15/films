package com.sber.films.FilmLibrary.repository;

import com.sber.films.FilmLibrary.model.Order;
import com.sber.films.FilmLibrary.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends GenericRepository<Order>{
    List<Order> findByUser(User user);
}
