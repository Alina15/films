package com.sber.films.FilmLibrary.repository;

import com.sber.films.FilmLibrary.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<User>{
    @Query(nativeQuery = true,
            value = """
                select *
                from users
                where upper(login) = upper(:login)
                and is_deleted = false
            """)
    User findUserByLoginAndDeletedFalse(final String login);

}

