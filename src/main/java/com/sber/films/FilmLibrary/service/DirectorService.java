package com.sber.films.FilmLibrary.service;

import com.sber.films.FilmLibrary.dto.AddFilmOrDirectorDto;
import com.sber.films.FilmLibrary.dto.DirectorDto;
import com.sber.films.FilmLibrary.dto.create.CreateDirectorRequest;
import com.sber.films.FilmLibrary.exception.NotFoundException;
import com.sber.films.FilmLibrary.model.Director;
import com.sber.films.FilmLibrary.repository.DirectorRepository;
import org.springframework.stereotype.Service;
import com.sber.films.FilmLibrary.mapper.DirectorMapper;
import java.util.Collections;

@Service
public class DirectorService extends GenericService<Director, DirectorDto>{
    public DirectorService(DirectorRepository repository,
                           DirectorMapper mapper) {
        super(repository, mapper);
    }

    public DirectorDto createDirectorByRequest(final CreateDirectorRequest createDirectorRequest) {
        DirectorDto directorDto = new DirectorDto();
        directorDto.setDirectorFio(createDirectorRequest.getDirectorFio());
        directorDto.setPosition(createDirectorRequest.getPosition());
        directorDto.setFilmIds(createDirectorRequest.getFilmIds());
        return super.create(directorDto);
    }

    public DirectorDto addFilm(final AddFilmOrDirectorDto addDirectorDto) throws NotFoundException {
        DirectorDto directorDto = getOne(addDirectorDto.getDirectorId());
        System.out.println(directorDto.getFilmIds());
        if (directorDto.getFilmIds().isEmpty()){
            directorDto.setFilmIds(Collections.singletonList(addDirectorDto.getFilmId()));
        }else {
            directorDto.getFilmIds().add(addDirectorDto.getFilmId());
        }
        update(directorDto, addDirectorDto.getDirectorId());
        return directorDto;
    }
}
