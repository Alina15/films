package com.sber.films.FilmLibrary.service;

import com.sber.films.FilmLibrary.dto.AddFilmOrDirectorDto;
import com.sber.films.FilmLibrary.dto.FilmDto;
import com.sber.films.FilmLibrary.dto.create.CreateFilmRequest;
import com.sber.films.FilmLibrary.exception.NotFoundException;
import com.sber.films.FilmLibrary.mapper.FilmMapper;
import com.sber.films.FilmLibrary.model.Film;
import com.sber.films.FilmLibrary.model.Order;
import com.sber.films.FilmLibrary.model.User;
import com.sber.films.FilmLibrary.repository.FilmRepository;
import com.sber.films.FilmLibrary.repository.OrderRepository;
import com.sber.films.FilmLibrary.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class FilmService extends GenericService<Film, FilmDto>{

        public FilmService(FilmRepository repository,
                           FilmMapper mapper) {
            super(repository, mapper);

        }

        public FilmDto createFilmByRequest(final CreateFilmRequest createFilmRequest) {
            FilmDto filmDto = new FilmDto();
            filmDto.setTitle(createFilmRequest.getTitle());
            filmDto.setGenre(createFilmRequest.getGenre());
            filmDto.setPremierYear(createFilmRequest.getPremierYear());
            filmDto.setCountry(createFilmRequest.getCountry());
            filmDto.setDirectorIds(createFilmRequest.getDirectorIds());
            return super.create(filmDto);
        }

        public FilmDto addDirector(final AddFilmOrDirectorDto addFilmOrDirectorDto) throws NotFoundException {
            FilmDto filmDto = getOne(addFilmOrDirectorDto.getFilmId());
            System.out.println(filmDto.getDirectorIds());
            if (filmDto.getDirectorIds().isEmpty()){
                filmDto.setDirectorIds(Collections.singletonList(addFilmOrDirectorDto.getDirectorId()));
            }else {
                filmDto.getDirectorIds().add(addFilmOrDirectorDto.getDirectorId());
            }
            update(filmDto, addFilmOrDirectorDto.getFilmId());
            return filmDto;
        }
    }
