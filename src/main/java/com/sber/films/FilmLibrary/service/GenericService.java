package com.sber.films.FilmLibrary.service;

import com.sber.films.FilmLibrary.dto.GenericDto;
import com.sber.films.FilmLibrary.exception.NotFoundException;
import com.sber.films.FilmLibrary.mapper.GenericMapperImpl;
import com.sber.films.FilmLibrary.model.GenericModel;
import com.sber.films.FilmLibrary.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public abstract class GenericService<T extends GenericModel, N extends GenericDto> {
    protected final GenericRepository<T> repository;
    protected final GenericMapperImpl<T, N> mapper;

    public GenericService(GenericRepository<T> repository,
                          GenericMapperImpl<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<N> listAll() {
        return mapper.toDtos(repository.findAll());
    }

    public N getOne(final Long id) throws NotFoundException {
        return mapper.toDto(repository.findById(id).orElseThrow(() -> new NotFoundException("Данных не найдено по заданному id")));
    }

    public N create(N newEntity) {
        newEntity.setCreatedWhen(LocalDateTime.now());
        // TODO: сделать авторизацию
        newEntity.setCreatedBy("ADMIN");
        T entity = mapper.toEntity(newEntity);
        return mapper.toDto(repository.save(entity));
    }

    public N update(N updatedEntity, final Long id) {
        // TODO: сделать авторизацию
        updatedEntity.setUpdatedBy("ADMIN");
        updatedEntity.setUpdatedWhen(LocalDateTime.now());
        updatedEntity.setId(id);
        return mapper.toDto(repository.save(mapper.toEntity(updatedEntity)));
    }

    public void delete(final Long id) {
        repository.deleteById(id);
    }
}

