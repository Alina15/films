package com.sber.films.FilmLibrary.service;

import com.sber.films.FilmLibrary.dto.FilmDto;
import com.sber.films.FilmLibrary.dto.OrderDto;
import com.sber.films.FilmLibrary.dto.UserDto;
import com.sber.films.FilmLibrary.dto.create.CreateOrderRequest;
import com.sber.films.FilmLibrary.dto.create.CreateUserRequest;
import com.sber.films.FilmLibrary.exception.NotFoundException;
import com.sber.films.FilmLibrary.mapper.OrderMapper;
import com.sber.films.FilmLibrary.mapper.UserMapper;
import com.sber.films.FilmLibrary.model.Order;
import com.sber.films.FilmLibrary.model.User;
import com.sber.films.FilmLibrary.repository.OrderRepository;
import com.sber.films.FilmLibrary.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService extends GenericService<Order, OrderDto>{
    private final UserService userService;
    private final FilmService filmService;
    public OrderService(OrderRepository repository,
                        OrderMapper mapper, UserService userService, FilmService filmService) {
        super(repository, mapper);
        this.filmService = filmService;
        this.userService = userService;
    }

    public OrderDto createOrderByRequest(final CreateOrderRequest newOrderDto) {
        OrderDto orderDto = new OrderDto();
        orderDto.setRentDate(newOrderDto.getRentDate());
        orderDto.setReturnPeriod(newOrderDto.getReturnPeriod());
        orderDto.setPurchase(newOrderDto.getPurchase());
        orderDto.setFilmId(newOrderDto.getFilmId());
        orderDto.setUserId(newOrderDto.getUserId());
        return super.create(orderDto);
    }
    public List<FilmDto> getOrderedFilmsByUser(Long userId) throws NotFoundException {
   UserDto user = userService.getOne(userId);
        List<Order> orders = super.repository.findAllById(user.getOrderIds());
        List<FilmDto> orderedFilms = new ArrayList<>();
        for (OrderDto order : super.mapper.toDtos(orders)) {
            orderedFilms.add(filmService.getOne(order.getFilmId()));
        }
        return orderedFilms;
    }
}
