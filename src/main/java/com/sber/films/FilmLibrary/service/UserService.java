package com.sber.films.FilmLibrary.service;

import com.sber.films.FilmLibrary.dto.RoleDto;
import com.sber.films.FilmLibrary.dto.UserDto;
import com.sber.films.FilmLibrary.dto.create.CreateUserRequest;
import com.sber.films.FilmLibrary.mapper.UserMapper;
import com.sber.films.FilmLibrary.model.User;
import com.sber.films.FilmLibrary.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService extends GenericService<User, UserDto>{
    @Value("${spring.security.user.name}")
    private String adminUser;
    private final BCryptPasswordEncoder passwordEncoder;

    public UserService(UserRepository repository,
                       UserMapper mapper, BCryptPasswordEncoder passwordEncoder) {
        super(repository, mapper);
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDto create(UserDto userDto) {
        RoleDto roleDto = new RoleDto();
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if (adminUser.equals(userName)) {
            roleDto.setId(2L);
        } else {
            roleDto.setId(1L);
        }
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userDto.setRoleId(roleDto.getId());
        return super.create(userDto);
    }
        public boolean checkPassword(final String passwordToCheck,
                                 final UserDetails userDetails) {
        return passwordEncoder.matches(passwordToCheck, userDetails.getPassword());
    }


}
