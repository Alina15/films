package com.sber.films.FilmLibrary.controller.rest;

import com.sber.films.FilmLibrary.controller.GenericControllerTest;
import com.sber.films.FilmLibrary.dto.AddFilmOrDirectorDto;
import com.sber.films.FilmLibrary.dto.DirectorDto;
import com.sber.films.FilmLibrary.dto.FilmDto;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;

import static com.sber.films.FilmLibrary.model.Genre.DRAMA;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class FilmControllerTest extends GenericControllerTest {
    @Test
    @Order(4)
    @Override
    protected void createObject() throws Exception {
        FilmDto filmDto = new FilmDto("createTest", LocalDate.now(), DRAMA, "USA", null, null);
        String result = mvc.perform(
                        MockMvcRequestBuilders.post("/films/addFilm")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(asJsonString(filmDto)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }
    @Test
    @Order(3)
    @Override
    protected void updateObject() throws Exception {
        FilmDto filmDto = new FilmDto("createTest", LocalDate.now(), DRAMA, "RUSSIA", null, null);
        String result = mvc.perform(
                        MockMvcRequestBuilders.put("/films/updateFilm?id=4")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(asJsonString(filmDto)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    @Test
    @Order(5)
    @Override
    protected void deleteObject() throws Exception {
        String result = mvc.perform(
                        MockMvcRequestBuilders.delete("/films/deleteFilm/4")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    @Test
    @Order(1)
    @Override
    protected void listAll() throws Exception {
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/films/list")
//                        .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

    }
    @Test
    @Order(2)
    @Override
    protected void listOne() throws Exception {
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/films/getFilm?id=1")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }
    @Test
    @Order(6)
    protected void addDirector() throws Exception {
        AddFilmOrDirectorDto addFilmOrDirectorDto = new AddFilmOrDirectorDto(4L,1L);
        String result = mvc.perform(
                        MockMvcRequestBuilders.post("/films/addDirector")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(asJsonString(addFilmOrDirectorDto)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }
}
