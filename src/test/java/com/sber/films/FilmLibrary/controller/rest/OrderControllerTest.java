package com.sber.films.FilmLibrary.controller.rest;

import com.sber.films.FilmLibrary.controller.GenericControllerTest;
import com.sber.films.FilmLibrary.dto.AddFilmOrDirectorDto;
import com.sber.films.FilmLibrary.dto.DirectorDto;
import com.sber.films.FilmLibrary.dto.OrderDto;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OrderControllerTest extends GenericControllerTest {
    @Test
    @Order(4)
    @Override
    protected void createObject() throws Exception {
        OrderDto orderDto = new OrderDto(LocalDate.now(),
                LocalDate.now(),
                false,
                1L,
                1L);
        String result = mvc.perform(
                        MockMvcRequestBuilders.post("/order/addOrder")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(asJsonString(orderDto)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }
    @Test
    @Order(3)
    @Override
    protected void updateObject() throws Exception {
        OrderDto orderDto = new OrderDto(LocalDate.now(),
                LocalDate.now(),
                false,
                2L,
                1L);
        String result = mvc.perform(
                        MockMvcRequestBuilders.put("/order/updateOrder?id=1")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(asJsonString(orderDto)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }
    @Test
    @Order(5)
    @Override
    protected void deleteObject() throws Exception {
        String result = mvc.perform(
                        MockMvcRequestBuilders.delete("/order/deleteOrder/1")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    @Test
    @Order(1)
    @Override
    protected void listAll() throws Exception {
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/order/list")
//                        .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

    }
    @Test
    @Order(2)
    @Override
    protected void listOne() throws Exception {
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/order/getOrder?id=1")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }
    @Test
    @Order(6)
    protected void listOrderedFilmsByUser() throws Exception {
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/order/listOrderedFilmsByUser?id=5")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }
}
