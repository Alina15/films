package com.sber.films.FilmLibrary.controller.rest;

import com.sber.films.FilmLibrary.controller.GenericControllerTest;
import com.sber.films.FilmLibrary.dto.AddFilmOrDirectorDto;
import com.sber.films.FilmLibrary.dto.DirectorDto;
import com.sber.films.FilmLibrary.dto.UserDto;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.ArrayList;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest extends GenericControllerTest {
    @Test
    @Order(4)
    @Override
    protected void createObject() throws Exception {
        UserDto userDto = new UserDto("login1",
                "password1",
                "email1",
                "firstName1",
                "middleName1",
                "lastName1",
                LocalDate.now(),
                "phone1",
                "address1",
                1L,
                new ArrayList<>(1));
        String result = mvc.perform(
                        MockMvcRequestBuilders.post("/user/addUser")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(asJsonString(userDto)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }
    @Test
    @Order(3)
    @Override
    protected void updateObject() throws Exception {
        UserDto userDto = new UserDto("loginUpdate",
                "password1",
                "email1",
                "firstName1",
                "middleName1",
                "lastName1",
                LocalDate.now(),
                "phone1",
                "address1",
                1L,
                new ArrayList<>(1));
        String result = mvc.perform(
                        MockMvcRequestBuilders.put("/user/updateUser?id=6")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .content(asJsonString(userDto)))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    @Test
    @Order(5)
    @Override
    protected void deleteObject() throws Exception {
        String result = mvc.perform(
                        MockMvcRequestBuilders.delete("/user/deleteUser/6")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    @Test
    @Order(1)
    @Override
    protected void listAll() throws Exception {
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/user/list")
//                        .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

    }
    @Test
    @Order(2)
    @Override
    protected void listOne() throws Exception {
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/user/getUser?id=1")
                                //                       .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

}
