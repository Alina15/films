package com.sber.films.FilmLibrary.service;

import com.sber.films.FilmLibrary.dto.AddFilmOrDirectorDto;
import com.sber.films.FilmLibrary.dto.DirectorDto;
import com.sber.films.FilmLibrary.exception.NotFoundException;
import com.sber.films.FilmLibrary.mapper.DirectorMapper;
import com.sber.films.FilmLibrary.model.Director;
import com.sber.films.FilmLibrary.repository.DirectorRepository;
import com.sber.films.FilmLibrary.service.data.DirectorTestData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class DirectorServiceTest extends GenericTest<Director, DirectorDto> {
    public DirectorServiceTest() {
        repository = Mockito.mock(DirectorRepository.class);
        mapper = Mockito.mock(DirectorMapper.class);
        service = new DirectorService((DirectorRepository) repository, (DirectorMapper) mapper);
    }

    @Order(3)
    @Test
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll())
                .thenReturn(DirectorTestData.Director_LIST);
        Mockito.when(mapper.toDtos(DirectorTestData.Director_LIST))
                .thenReturn(DirectorTestData.Director_DTO_LIST);
        List<DirectorDto> DirectorDtos = service.listAll();
        log.info("Testing getAll(): " + DirectorDtos.toString());
        assertEquals(DirectorTestData.Director_LIST.size(), DirectorDtos.size());
    }

    @Order(2)
    @Test
    @Override
    protected void getOne() throws NotFoundException {
        Director Director1 = DirectorTestData.Director_1;
        Director1.setId(1L);

        DirectorDto DirectorDtoInitial = DirectorTestData.Director_DTO_1;
        DirectorDtoInitial.setId(1L);

        Director Director2 = DirectorTestData.Director_2;
        Director2.setId(2L);

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(Director1));
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(Director2));
        Mockito.when(mapper.toDto(Director1)).thenReturn(DirectorDtoInitial);
        Mockito.when(mapper.toDto(Director2)).thenReturn(DirectorTestData.Director_DTO_2);

        DirectorDto DirectorDto1 = service.getOne(1L);
        DirectorDto DirectorDto2 = service.getOne(2L);
        log.info("Testing getOne(): " + DirectorDto1);
        log.info("Testing getOne(): " + DirectorDto2);
        assertEquals(DirectorDto1, DirectorTestData.Director_DTO_1);
        assertNotEquals(DirectorDto2, DirectorTestData.Director_DTO_1);
    }

    @Order(1)
    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(DirectorTestData.Director_DTO_1)).thenReturn(DirectorTestData.Director_1);
        Mockito.when(mapper.toDto(DirectorTestData.Director_1)).thenReturn(DirectorTestData.Director_DTO_1);
        Mockito.when(repository.save(DirectorTestData.Director_1)).thenReturn(DirectorTestData.Director_1);
        DirectorDto DirectorDto = service.create(DirectorTestData.Director_DTO_1);
        log.info("Testing create(): " + DirectorDto);
        assertEquals(DirectorDto, DirectorTestData.Director_DTO_1);
    }

    @Order(4)
    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(DirectorTestData.Director_DTO_1)).thenReturn(DirectorTestData.Director_1);
        Mockito.when(mapper.toDto(DirectorTestData.Director_1)).thenReturn(DirectorTestData.Director_DTO_1);
        Mockito.when(repository.save(DirectorTestData.Director_1)).thenReturn(DirectorTestData.Director_1);
        DirectorDto DirectorDto = service.update(DirectorTestData.Director_DTO_1, 1L);
        log.info("Testing update(): " + DirectorDto);
        assertEquals(DirectorDto, DirectorTestData.Director_DTO_1);
    }

    @Order(5)
    @Test
    @Override
    protected void delete() {
//        Mockito.d(repository.deleteById(3L)).thenReturn(DirectorTestData.Director_DTO_3_DELETED);
//        service.delete(3L);
    }

    @Test
    @Order(6)
    void addFilm() throws NotFoundException {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(DirectorTestData.Director_1));
        Mockito.when(service.getOne(1L)).thenReturn(DirectorTestData.Director_DTO_1);
        Mockito.when(repository.save(DirectorTestData.Director_1)).thenReturn(DirectorTestData.Director_1);
        ((DirectorService) service).addFilm(new AddFilmOrDirectorDto(1L, 1L));
        log.info("Testing addFilm(): " + DirectorTestData.Director_DTO_1.getFilmIds());
        assertFalse(DirectorTestData.Director_DTO_1.getFilmIds().isEmpty());
    }

}
