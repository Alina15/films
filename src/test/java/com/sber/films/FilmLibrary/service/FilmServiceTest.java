package com.sber.films.FilmLibrary.service;

import com.sber.films.FilmLibrary.dto.AddFilmOrDirectorDto;
import com.sber.films.FilmLibrary.dto.DirectorDto;
import com.sber.films.FilmLibrary.dto.FilmDto;
import com.sber.films.FilmLibrary.exception.NotFoundException;
import com.sber.films.FilmLibrary.mapper.FilmMapper;
import com.sber.films.FilmLibrary.model.Director;
import com.sber.films.FilmLibrary.model.Film;
import com.sber.films.FilmLibrary.repository.FilmRepository;
import com.sber.films.FilmLibrary.service.data.FilmTestData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@Slf4j
public class FilmServiceTest extends GenericTest<Film, FilmDto>{
    public FilmServiceTest() {
        repository = Mockito.mock(FilmRepository.class);
        mapper = Mockito.mock(FilmMapper.class);
        service = new FilmService((FilmRepository) repository, (FilmMapper) mapper);
    }

    @Order(3)
    @Test
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll())
                .thenReturn(FilmTestData.Film_LIST);
        Mockito.when(mapper.toDtos(FilmTestData.Film_LIST))
                .thenReturn(FilmTestData.Film_DTO_LIST);
        List<FilmDto> FilmDtos = service.listAll();
        log.info("Testing getAll(): " + FilmDtos.toString());
        assertEquals(FilmTestData.Film_LIST.size(), FilmDtos.size());
    }

    @Order(2)
    @Test
    @Override
    protected void getOne() throws NotFoundException {
        Film Film1 = FilmTestData.Film_1;
        Film1.setId(1L);

        FilmDto FilmDtoInitial = FilmTestData.Film_DTO_1;
        FilmDtoInitial.setId(1L);

        Film Film2 = FilmTestData.Film_2;
        Film2.setId(2L);

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(Film1));
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(Film2));
        Mockito.when(mapper.toDto(Film1)).thenReturn(FilmDtoInitial);
        Mockito.when(mapper.toDto(Film2)).thenReturn(FilmTestData.Film_DTO_2);

        FilmDto FilmDto1 = service.getOne(1L);
        FilmDto FilmDto2 = service.getOne(2L);
        log.info("Testing getOne(): " + FilmDto1);
        log.info("Testing getOne(): " + FilmDto2);
        assertEquals(FilmDto1, FilmTestData.Film_DTO_1);
        assertNotEquals(FilmDto2, FilmTestData.Film_DTO_1);
    }

    @Order(1)
    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(FilmTestData.Film_DTO_1)).thenReturn(FilmTestData.Film_1);
        Mockito.when(mapper.toDto(FilmTestData.Film_1)).thenReturn(FilmTestData.Film_DTO_1);
        Mockito.when(repository.save(FilmTestData.Film_1)).thenReturn(FilmTestData.Film_1);
        FilmDto FilmDto = service.create(FilmTestData.Film_DTO_1);
        log.info("Testing create(): " + FilmDto);
        assertEquals(FilmDto, FilmTestData.Film_DTO_1);
    }

    @Order(4)
    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(FilmTestData.Film_DTO_1)).thenReturn(FilmTestData.Film_1);
        Mockito.when(mapper.toDto(FilmTestData.Film_1)).thenReturn(FilmTestData.Film_DTO_1);
        Mockito.when(repository.save(FilmTestData.Film_1)).thenReturn(FilmTestData.Film_1);
        FilmDto FilmDto = service.update(FilmTestData.Film_DTO_1, 1L);
        log.info("Testing update(): " + FilmDto);
        assertEquals(FilmDto, FilmTestData.Film_DTO_1);
    }

    @Order(5)
    @Test
    @Override
    protected void delete() {
//        Mockito.d(repository.deleteById(3L)).thenReturn(FilmTestData.Film_DTO_3_DELETED);
//        service.delete(3L);
    }

    @Test
    @Order(6)
    void addDirector() throws NotFoundException {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(FilmTestData.Film_1));
        Mockito.when(service.getOne(1L)).thenReturn(FilmTestData.Film_DTO_1);
        Mockito.when(repository.save(FilmTestData.Film_1)).thenReturn(FilmTestData.Film_1);
        ((FilmService) service).addDirector(new AddFilmOrDirectorDto(1L, 1L));
        log.info("Testing addDirector(): " + FilmTestData.Film_DTO_1.getDirectorIds());
        assertFalse(FilmTestData.Film_DTO_1.getDirectorIds().isEmpty());
    }

}
