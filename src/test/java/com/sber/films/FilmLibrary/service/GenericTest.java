package com.sber.films.FilmLibrary.service;

import com.sber.films.FilmLibrary.dto.GenericDto;
import com.sber.films.FilmLibrary.exception.NotFoundException;
import com.sber.films.FilmLibrary.mapper.GenericMapperImpl;
import com.sber.films.FilmLibrary.model.GenericModel;
import com.sber.films.FilmLibrary.repository.GenericRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public abstract class GenericTest<E extends GenericModel, D extends GenericDto> {
    protected GenericService<E, D> service;
    protected GenericRepository<E> repository;
    protected GenericMapperImpl<E, D> mapper;

    @BeforeEach
    void init() {
        //TODO: добавить логику на авторизацию, как только она будет готова
    }

    protected abstract void getAll();

    protected abstract void getOne() throws NotFoundException;

    protected abstract void create();

    protected abstract void update();

    protected abstract void delete();
}

