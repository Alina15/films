package com.sber.films.FilmLibrary.service;

import com.sber.films.FilmLibrary.dto.FilmDto;
import com.sber.films.FilmLibrary.dto.OrderDto;
import com.sber.films.FilmLibrary.dto.UserDto;
import com.sber.films.FilmLibrary.exception.NotFoundException;
import com.sber.films.FilmLibrary.mapper.OrderMapper;
import com.sber.films.FilmLibrary.model.Order;
import com.sber.films.FilmLibrary.model.User;
import com.sber.films.FilmLibrary.repository.OrderRepository;
import com.sber.films.FilmLibrary.service.data.FilmTestData;
import com.sber.films.FilmLibrary.service.data.OrderTestData;
import com.sber.films.FilmLibrary.service.data.UserTestData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class OrderServiceTest extends GenericTest<Order, OrderDto>{
    private final UserService userService;
    private final FilmService filmService;
    public OrderServiceTest() {
        repository = Mockito.mock(OrderRepository.class);
        mapper = Mockito.mock(OrderMapper.class);
        userService = Mockito.mock(UserService.class);
        filmService = Mockito.mock(FilmService.class);
        service = new OrderService((OrderRepository) repository, (OrderMapper) mapper, (UserService) userService, (FilmService) filmService);
    }


    @Test
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll())
                .thenReturn(OrderTestData.Order_LIST);
        Mockito.when(mapper.toDtos(OrderTestData.Order_LIST))
                .thenReturn(OrderTestData.Order_DTO_LIST);
        List<OrderDto> OrderDtos = service.listAll();
        log.info("Testing getAll(): " + OrderDtos.toString());
        assertEquals(OrderTestData.Order_LIST.size(), OrderDtos.size());
    }


    @Test
    @Override
    protected void getOne() throws NotFoundException {
        Order Order1 = OrderTestData.Order_1;
        Order1.setId(1L);

        OrderDto OrderDtoInitial = OrderTestData.Order_DTO_1;
        OrderDtoInitial.setId(1L);

        Order Order2 = OrderTestData.Order_2;
        Order2.setId(2L);

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(Order1));
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(Order2));
        Mockito.when(mapper.toDto(Order1)).thenReturn(OrderDtoInitial);
        Mockito.when(mapper.toDto(Order2)).thenReturn(OrderTestData.Order_DTO_2);

        OrderDto OrderDto1 = service.getOne(1L);
        OrderDto OrderDto2 = service.getOne(2L);
        log.info("Testing getOne(): " + OrderDto1);
        log.info("Testing getOne(): " + OrderDto2);
        assertEquals(OrderDto1, OrderTestData.Order_DTO_1);
        assertNotEquals(OrderDto2, OrderTestData.Order_DTO_1);
    }


    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(OrderTestData.Order_DTO_1)).thenReturn(OrderTestData.Order_1);
        Mockito.when(mapper.toDto(OrderTestData.Order_1)).thenReturn(OrderTestData.Order_DTO_1);
        Mockito.when(repository.save(OrderTestData.Order_1)).thenReturn(OrderTestData.Order_1);
        OrderDto OrderDto = service.create(OrderTestData.Order_DTO_1);
        log.info("Testing create(): " + OrderDto);
        assertEquals(OrderDto, OrderTestData.Order_DTO_1);
    }


    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(OrderTestData.Order_DTO_1)).thenReturn(OrderTestData.Order_1);
        Mockito.when(mapper.toDto(OrderTestData.Order_1)).thenReturn(OrderTestData.Order_DTO_1);
        Mockito.when(repository.save(OrderTestData.Order_1)).thenReturn(OrderTestData.Order_1);
        OrderDto OrderDto = service.update(OrderTestData.Order_DTO_1, 1L);
        log.info("Testing update(): " + OrderDto);
        assertEquals(OrderDto, OrderTestData.Order_DTO_1);
    }


    @Test
    @Override
    protected void delete() {
//        Mockito.d(repository.deleteById(3L)).thenReturn(OrderTestData.Order_DTO_3_DELETED);
//        service.delete(3L);
    }

    @Test
    void getOrderedFilmsByUser() throws NotFoundException {
        Mockito.when(userService.getOne(1L)).thenReturn(UserTestData.User_DTO_1);
        Mockito.when(repository.findAllById(UserTestData.User_DTO_1.getOrderIds())).thenReturn(OrderTestData.Order_LIST_BY_FILM);
        Mockito.when(mapper.toDtos(OrderTestData.Order_LIST_BY_FILM)).thenReturn(OrderTestData.Order_DTO_LIST_BY_FILM);
        Mockito.when(filmService.getOne(1L)).thenReturn(FilmTestData.Film_DTO_1);

        List<FilmDto> filmDtos = ((OrderService) service).getOrderedFilmsByUser(1L);
        log.info("Testing getOrderedFilmsByUser(): " + filmDtos);
        assertEquals(filmDtos, FilmTestData.Film_DTO_BY_ORDER);
    }

}
