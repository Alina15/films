package com.sber.films.FilmLibrary.service;

import com.sber.films.FilmLibrary.dto.UserDto;
import com.sber.films.FilmLibrary.exception.NotFoundException;
import com.sber.films.FilmLibrary.mapper.UserMapper;
import com.sber.films.FilmLibrary.model.User;
import com.sber.films.FilmLibrary.repository.UserRepository;
import com.sber.films.FilmLibrary.service.data.UserTestData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@Slf4j
public class UserServiceTest extends GenericTest<User, UserDto>{
    public UserServiceTest() {
        repository = Mockito.mock(UserRepository.class);
        mapper = Mockito.mock(UserMapper.class);
        //service = new UserService((UserRepository) repository, (UserMapper) mapper, );
    }

    @Order(3)
    @Test
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll())
                .thenReturn(UserTestData.User_LIST);
        Mockito.when(mapper.toDtos(UserTestData.User_LIST))
                .thenReturn(UserTestData.User_DTO_LIST);
        List<UserDto> UserDtos = service.listAll();
        log.info("Testing getAll(): " + UserDtos.toString());
        assertEquals(UserTestData.User_LIST.size(), UserDtos.size());
    }

    @Order(2)
    @Test
    @Override
    protected void getOne() throws NotFoundException {
        User User1 = UserTestData.User_1;
        User1.setId(1L);

        UserDto UserDtoInitial = UserTestData.User_DTO_1;
        UserDtoInitial.setId(1L);

        User User2 = UserTestData.User_2;
        User2.setId(2L);

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(User1));
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(User2));
        Mockito.when(mapper.toDto(User1)).thenReturn(UserDtoInitial);
        Mockito.when(mapper.toDto(User2)).thenReturn(UserTestData.User_DTO_2);

        UserDto UserDto1 = service.getOne(1L);
        UserDto UserDto2 = service.getOne(2L);
        log.info("Testing getOne(): " + UserDto1);
        log.info("Testing getOne(): " + UserDto2);
        assertEquals(UserDto1, UserTestData.User_DTO_1);
        assertNotEquals(UserDto2, UserTestData.User_DTO_1);
    }

    @Order(1)
    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(UserTestData.User_DTO_1)).thenReturn(UserTestData.User_1);
        Mockito.when(mapper.toDto(UserTestData.User_1)).thenReturn(UserTestData.User_DTO_1);
        Mockito.when(repository.save(UserTestData.User_1)).thenReturn(UserTestData.User_1);
        UserDto UserDto = service.create(UserTestData.User_DTO_1);
        log.info("Testing create(): " + UserDto);
        assertEquals(UserDto, UserTestData.User_DTO_1);
    }

    @Order(4)
    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(UserTestData.User_DTO_1)).thenReturn(UserTestData.User_1);
        Mockito.when(mapper.toDto(UserTestData.User_1)).thenReturn(UserTestData.User_DTO_1);
        Mockito.when(repository.save(UserTestData.User_1)).thenReturn(UserTestData.User_1);
        UserDto UserDto = service.update(UserTestData.User_DTO_1, 1L);
        log.info("Testing update(): " + UserDto);
        assertEquals(UserDto, UserTestData.User_DTO_1);
    }

    @Order(5)
    @Test
    @Override
    protected void delete() {
//        Mockito.d(repository.deleteById(3L)).thenReturn(UserTestData.User_DTO_3_DELETED);
//        service.delete(3L);
    }


}
