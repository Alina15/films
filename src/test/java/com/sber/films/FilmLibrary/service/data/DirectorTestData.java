package com.sber.films.FilmLibrary.service.data;

import com.sber.films.FilmLibrary.dto.DirectorDto;
import com.sber.films.FilmLibrary.model.Director;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface DirectorTestData {
    DirectorDto Director_DTO_1 = new DirectorDto("DirectorFio1",
            "position1",
            new ArrayList<>());

    DirectorDto Director_DTO_2 = new DirectorDto("DirectorFio2",
            "position2",
            new ArrayList<>());

    DirectorDto Director_DTO_3_DELETED = new DirectorDto("DirectorFio3",
            "position3",
            new ArrayList<>());

    List<DirectorDto> Director_DTO_LIST = Arrays.asList(Director_DTO_1, Director_DTO_2, Director_DTO_3_DELETED);


    Director Director_1 = new Director("Director1",
            "position1",
            null);

    Director Director_2 = new Director("Director2",
            "position2",
            null);

    Director Director_3 = new Director("Director3",
            "position3",
            null);

    List<Director> Director_LIST = Arrays.asList(Director_1, Director_2, Director_3);

}
