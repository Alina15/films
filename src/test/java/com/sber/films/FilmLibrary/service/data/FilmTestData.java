package com.sber.films.FilmLibrary.service.data;

import com.sber.films.FilmLibrary.dto.FilmDto;
import com.sber.films.FilmLibrary.model.Film;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.sber.films.FilmLibrary.model.Genre.FANTASY;

public interface FilmTestData {
    FilmDto Film_DTO_1 = new FilmDto("FilmTitle1",
            LocalDate.now(),
            FANTASY,
            "USA",
            new ArrayList<>(1),
            new ArrayList<>());

    FilmDto Film_DTO_2 = new FilmDto("FilmTitle2",
            LocalDate.now(),
            FANTASY,
            "USA",
            new ArrayList<>(1),
            new ArrayList<>());

    FilmDto Film_DTO_3_DELETED = new FilmDto("FilmTitle3",
            LocalDate.now(),
            FANTASY,
            "USA",
            new ArrayList<>(),
            new ArrayList<>());

    List<FilmDto> Film_DTO_LIST = Arrays.asList(Film_DTO_1, Film_DTO_2, Film_DTO_3_DELETED);
    List<FilmDto> Film_DTO_BY_ORDER = List.of(Film_DTO_1);

    Film Film_1 = new Film("Film1",
            LocalDate.now(),
            "USA",
            FANTASY,
            new ArrayList<>(1),
            null);

    Film Film_2 = new Film("Film2",
            LocalDate.now(),
            "USA",
            FANTASY,
            new ArrayList<>(1),
            null);

    Film Film_3 = new Film("Film3",
            LocalDate.now(),
            "USA",
            FANTASY,
            null,
            null);

    List<Film> Film_LIST = Arrays.asList(Film_1, Film_2, Film_3);

}
