package com.sber.films.FilmLibrary.service.data;

import com.sber.films.FilmLibrary.dto.OrderDto;
import com.sber.films.FilmLibrary.model.Order;
import jakarta.validation.constraints.Null;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface OrderTestData {
    OrderDto Order_DTO_1 = new OrderDto(LocalDate.now(),
            LocalDate.now(),
            false,
            1L,
            1L);

    OrderDto Order_DTO_2 = new OrderDto(LocalDate.now(),
            LocalDate.now(),
            false,
            2L,
            1L);

    OrderDto Order_DTO_3_DELETED = new OrderDto(LocalDate.now(),
            LocalDate.now(),
            false,
            1L,
            2L);

    List<OrderDto> Order_DTO_LIST = Arrays.asList(Order_DTO_1, Order_DTO_2, Order_DTO_3_DELETED);
    List<OrderDto> Order_DTO_LIST_BY_FILM = List.of(Order_DTO_1);

    Order Order_1 = new Order(UserTestData.User_1,
            FilmTestData.Film_1,
            LocalDate.now(),
            LocalDate.now(),
            false);

    Order Order_2 = new Order(null,
            null,
            LocalDate.now(),
            LocalDate.now(),
            false);

    Order Order_3 = new Order(null,
            null,
            LocalDate.now(),
            LocalDate.now(),
            false);

    List<Order> Order_LIST = Arrays.asList(Order_1, Order_2, Order_3);
    List<Order> Order_LIST_BY_FILM = List.of(Order_1);
}
