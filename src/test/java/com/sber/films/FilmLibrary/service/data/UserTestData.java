package com.sber.films.FilmLibrary.service.data;

import com.sber.films.FilmLibrary.dto.UserDto;
import com.sber.films.FilmLibrary.model.User;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface UserTestData {
    UserDto User_DTO_1 = new UserDto("login1",
            "password1",
            "email1",
            "firstName1",
            "middleName1",
            "lastName1",
            LocalDate.now(),
            "phone1",
            "address1",
            1L,
            new ArrayList<>(1));

    UserDto User_DTO_2 = new UserDto("login2",
            "password2",
            "email2",
            "firstName2",
            "middleName2",
            "lastName2",
            LocalDate.now(),
            "phone2",
            "address2",
            1L,
            new ArrayList<>());

    UserDto User_DTO_3_DELETED = new UserDto("login3",
            "password3",
            "email3",
            "firstName3",
            "middleName3",
            "lastName3",
            LocalDate.now(),
            "phone3",
            "address3",
            1L,
            new ArrayList<>());

    List<UserDto> User_DTO_LIST = Arrays.asList(User_DTO_1, User_DTO_2, User_DTO_3_DELETED);


    User User_1 = new User("login1",
            "password1",
            "email1",
            "firstName1",
            "middleName1",
            "lastName1",
            LocalDate.now(),
            "phone1",
            "address1",
            null,
            new ArrayList<>(1));

    User User_2 = new User("login2",
            "password2",
            "email2",
            "firstName2",
            "middleName2",
            "lastName2",
            LocalDate.now(),
            "phone2",
            "address2",
            null,
            new ArrayList<>());

    User User_3 = new User("login3",
            "password3",
            "email3",
            "firstName3",
            "middleName3",
            "lastName3",
            LocalDate.now(),
            "phone3",
            "address3",
            null,
            new ArrayList<>());

    List<User> User_LIST = Arrays.asList(User_1, User_2, User_3);

}
